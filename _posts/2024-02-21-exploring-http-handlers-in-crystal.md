---
layout: post
title: Exploring HTTP::Handlers in Crystal
date: 2024-02-21 15:20 +0100
---

# Exploring HTTP::Handlers in Crystal

HTTP::Handlers are a fundamental aspect of web development in Crystal, allowing developers to intercept and manipulate HTTP requests and responses. In this comprehensive guide, we'll dive deep into the world of HTTP::Handlers, exploring their purpose, usage, and how to create custom handlers to address specific needs.

## Understanding HTTP::Handlers

At its core, an HTTP::Handler is a component responsible for processing HTTP requests and responses within a web server. Handlers can perform various tasks such as logging, authentication, authorization, input validation, and more. Crystal provides a rich set of built-in handlers that cover many common use cases, but developers can also create custom handlers to extend the functionality of their web applications.

### Built-in HTTP::Handlers

Crystal's standard library includes several pre-defined HTTP::Handlers that developers can use out of the box:

- **LogHandler**: Logs incoming requests and outgoing responses, providing valuable insight into server activity.
- **StaticFileHandler**: Serves static files from a specified directory, such as HTML, CSS, JavaScript, and images.
- **CompressHandler**: Compresses responses to reduce bandwidth usage and improve website performance.
- **WebSocketHandler**: Add websocket functionality to `HTTP::Server`

These built-in handlers streamline common web development tasks and help developers build secure, efficient, and scalable web applications.

## Creating Custom HTTP::Handlers

While the built-in handlers cover many use cases, developers often encounter scenarios where custom handling logic is required. Fortunately, Crystal's flexible architecture makes it easy to create custom HTTP::Handlers tailored to specific requirements.

### Example: Bearer Token Authentication Handler

Let's walk through an example of creating a custom HTTP::Handler for bearer token authentication. This handler will intercept incoming requests, validate the bearer token in the `Authorization` header, and ensure that only authenticated users can access protected resources.

```crystal
require "http/server"

class BearerTokenAuthenticationHandler < HTTP::Handler
  def initialize(@token : String)
  end

  def call(context : HTTP::Server::Context)
    if context.request.headers["Authorization"] == "Bearer #{@token}"
      yield
    else
      context.response.status_code = 401
      context.response.print "Unauthorized"
    end
  end
end
```
