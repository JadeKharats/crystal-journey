---
layout: post
title: ECR library
date: 2024-02-20 23:40 +0100
---

ECR, which stands for Embedded Crystal, is a part of the Crystal standard library. It is designed to
facilitate the use of templates in your application. The goal is to generate text from data structures and
literals. In this article, we will discuss what ECR is, its purpose, and how to use it effectively.

What Is ECR?
--------------------
ECR was inspired by ERB (Embedded Ruby) and provides a similar functionality but for Crystal code instead of
Ruby. It allows you to embed Crystal code in your template files. You can then pass data structures and
literals to the templates, which will be rendered as text. This feature is particularly useful when it comes
to generating HTML or XML files dynamically.

Why Use ECR?
--------------------
One major advantage of using ECR is that you don't need to learn a new template language. With ECR, you can
use Crystal code directly in your templates, making them more readable and maintainable for Crystal
developers. Another benefit is that it provides better performance compared to other templating engines like
Mustache or Liquid.

Example of Usage
--------------------
To illustrate the usage of ECR, let's create a simple "Hello World" example. In your project directory, create
a new file called `hello_world.ecr`. Inside this file, add the following code:

```crystal
<h1>Hello <%= @name %></h1>
```

This is a basic HTML template that will greet a user with their name. The `<%= @name %>` part is an ECR syntax
that allows you to embed Crystal code in the template. This code will be evaluated when the template is
rendered and replaced by the value of `@name`.

Now, create a new file called `hello_world.cr`, which will contain our main program:

```crystal
require "ecr"

class HelloWorld
  property name : String = "World"
  ECR.def_to_s "hello_world.ecr"
end

hello = HelloWorld.new
puts hello.to_s
```

In this code, we require the `ecr` library and define a class called `HelloWorld`. Inside this class, we use
the `def_to_s` macro provided by ECR to specify that our template is in the file named "hello_world.ecr".

We then create an instance of `HelloWorld`, set its `@name` attribute to "World", and print it using `puts`.
This will render the template and output:

```html
<h1>Hello World</h1>
```

Conclusion
--------------------
ECR is a powerful tool that allows you to easily create templates in Crystal. By combining its functionality
with the power of Crystal's language, you can create dynamic text files without having to learn a new syntax
or rely on external libraries. I hope this article has helped you understand what ECR is and how it can be
used effectively in your projects.
