---
layout: page
title: About
permalink: /about/
---
# About Me

Hello everyone! 👋 My name is David YOTEAU and I'm excited to share my journey and passion for the Crystal programming language with you on this blog. 

I've been deeply involved in the tech industry for 20 years, with a focus on software enginering and quality assurance. Throughout my career, I've had the opportunity to work with various programming languages, but none have captured my attention and enthusiasm quite like Crystal-lang.

## My Journey with Crystal-lang

I first discovered Crystal-lang when I was looking for a way to compile ruby and make deployment easier. Since then, I've been on a journey of exploration and learning, diving into its elegant syntax, powerful features, and vibrant community.

Crystal-lang has not only changed the way I approach software development but has also inspired me to become an advocate and ambassador for the language. As an ambassador, my mission is to spread awareness and knowledge about Crystal-lang, helping developers discover its potential and embrace its benefits.

# My Role as a Crystal-lang Ambassador

## What is Crystal-lang?

For those who may be new to Crystal-lang, let me give you a brief overview. Crystal is a statically-typed, compiled programming language with a syntax inspired by Ruby but with the performance and type safety of languages like C or Rust. It's designed for building fast, reliable, and efficient software.

## My Responsibilities as an Ambassador

As a Crystal-lang ambassador, I take on several responsibilities:

- **Education and Outreach:** I strive to educate developers about the capabilities and advantages of Crystal-lang through articles, tutorials, talks, and workshops.
  
- **Community Building:** I actively engage with the Crystal community, participating in discussions, providing support, and fostering collaboration among developers.

- **Advocacy and Promotion:** I promote Crystal-lang at conferences, meetups, and online forums, advocating for its adoption and showcasing real-world use cases and success stories.

## Join Me on this Journey

I invite you to join me on this exciting journey as we explore the world of Crystal-lang together. Whether you're a seasoned developer looking to expand your skills or a curious beginner eager to learn something new, there's something here for everyone.

